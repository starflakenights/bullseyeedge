package com.nightsstarflake.bullseyeedge;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.File;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private WebView webView;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    private String defaultUA;
    private String desktopUA;
    private WebSettings webSettings;

    private void alertDiscontinued() {
        // Display the C&D dialog
        final AlertDialog.Builder internaldialog = new AlertDialog.Builder(this);
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Notice")
                .setMessage("At the request of Target Corporation, " +
                        "Bullseye Edge will be discontinued and " +
                        "removed from Google Play on 10/22/2016." + '\n' + '\n' +
                        "Please connect with your HR department for " +
                        "information regarding off-site access to any " +
                        "services linked within the app if you are unsure " +
                        "of the URLs (View Schedule, eHR, Outlook, " +
                        "Target Pay and Benefits, and Paperless Employee) " +
                        "or screenshot the following notice for reference.")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                internaldialog.setIcon(android.R.drawable.ic_dialog_alert);
                                internaldialog.setTitle("Access Information");
                                internaldialog.setMessage("Schedule: http://target.com/myschedule " + '\n' + '\n' +
                                                "eHR: http://itgtpb.target.com " + '\n' + '\n' +
                                                "Outlook: http://tgtmail.target.com " + '\n' + '\n' +
                                                "Pay and Benefits: http://targetpayandbenefits.com " + '\n' + '\n' +
                                                "Paperless Employee: http://paperlessemployee.com/target ");
                                internaldialog.setPositiveButton("Ok", null);
                                internaldialog.show();
                            }
                })
                .show();

        // Save the "newly discontinued" flag
        savePreferences("FLAG_isNewlyDiscontinued", false);
    }

    private void alertFirstRun() {
        // Display the first run dialog
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Notice")
                .setMessage("This app provides unofficial access to " +
                        "Target Corporation services such as " +
                        "team member schedules and email." + '\n' + '\n' +
                        "It does not collect any user information " +
                        "and its source is freely available, but you " +
                        "should always be mindful of the networks to " +
                        "which you connect and any information you share.")
                .setPositiveButton("Ok", null)
                .show();

        // Save the "first run" flag
        savePreferences("FLAG_isFirstRun", false);


    }

    private void setDefaultPreferences() {
        // Set the initial program flags
        savePreferences("pref_clearcache",  true);
        savePreferences("pref_savelogin", false);
        savePreferences("pref_showwebmail", false);
        savePreferences("pref_showbenefits", false);
        savePreferences("pref_showtaxes", false);
    }

    private void savePreferences(String key, boolean value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    private void savePreferences(String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private void loadSavedPreferences() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Determine if it's the first run - if so, initialize user preferences
        boolean bool_isFirstRun = sharedPreferences.getBoolean("FLAG_isFirstRun", true);

        if (bool_isFirstRun == true)
        {
            setDefaultPreferences();
            alertFirstRun();
        }

        // Let the user know about the C&D
        boolean bool_isDiscontinued = sharedPreferences.getBoolean("FLAG_isNewlyDiscontinued", true);

        if (bool_isDiscontinued == true)
        {
            alertDiscontinued();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Loads user preferences and displays the first run alert if it has not been seen
        loadSavedPreferences();

        webView = (WebView) findViewById(R.id.webview);

        webView.setWebChromeClient(new WebChromeClient());
        WebViewExtension webViewClient = new WebViewExtension(this, webView);

        webView.setWebViewClient(webViewClient);

        webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setCacheMode(webSettings.LOAD_NO_CACHE);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setDomStorageEnabled(true);

        // Set the user-agent to a desktop browser to fix weird mobile-only issues
        defaultUA = webSettings.getUserAgentString();
        desktopUA = getString(R.string.desktop_ua);
        webSettings.setUserAgentString(desktopUA);

        // Check to see if we need to allow saving login credentials
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Hide/Show Webmail
        boolean bool_saveLogin = sharedPreferences.getBoolean("pref_savelogin", true);
        webSettings.setSaveFormData(bool_saveLogin);
        webSettings.setSavePassword(bool_saveLogin);

        webView.loadUrl(getString(R.string.url_overview));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close){

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        // If we need to hide any menu items, hide them
        hideItems();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // First, try to close nav drawer
        if (drawer.isDrawerOpen(GravityCompat.START))
        {
            drawer.closeDrawer(GravityCompat.START);
        }

        // Next, try to go back in the WebView
        else if(webView.canGoBack() == true)
        {
            webView.goBack();
        }

        // Next, if WebView is all the way back, ask to exit
        else if(webView.canGoBack() == false){
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle("Preparing to Exit")
                    .setMessage("Are you sure you want to close "+getString(R.string.app_name)+"?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }

                    })
                    .setNegativeButton("No", null)
                    .show();
        }

        // Else, use normal back behavior
        else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.main, menu);

        hideItems();

        return true;
    }

    private void hideItems()
    {
        // If we need to hide any menu items, hide them
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        // Hide/Show Webmail
        boolean bool_showWebmail = sharedPreferences.getBoolean("pref_showwebmail", true);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        Menu navMenu = navigationView.getMenu();
        if (bool_showWebmail == false) {
            navMenu.findItem(R.id.nav_webmail).setVisible(false);
        }
        else {
            navMenu.findItem(R.id.nav_webmail).setVisible(true);
        }

        // Hide/Show Benefits
        boolean bool_showBenefits = sharedPreferences.getBoolean("pref_showbenefits", true);
        if (bool_showBenefits == false) {
            navMenu.findItem(R.id.nav_benefits).setVisible(false);
        }
        else {
            navMenu.findItem(R.id.nav_benefits).setVisible(true);
        }

        // Hide/Show Webmail
        boolean bool_showTaxes = sharedPreferences.getBoolean("pref_showtaxes", true);
        if (bool_showTaxes == false) {
            navMenu.findItem(R.id.nav_taxes).setVisible(false);
        }
        else {
            navMenu.findItem(R.id.nav_taxes).setVisible(true);
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        // If we need to hide any menu items, hide them
        hideItems();

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        // ID not required upon select
        // int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_schedule) {
            // Set the user-agent to the desktop UA to prevent weird display issues
            webSettings.setUserAgentString(desktopUA);

            // Load the schedule webview
            webView.loadUrl(getString(R.string.url_schedule));
        }
        else if (id == R.id.nav_eHR) {
            // Set the user-agent to the desktop UA to prevent weird display issues
            webSettings.setUserAgentString(desktopUA);

            // Load the eHR webview
            webView.loadUrl(getString(R.string.url_ehr));
        }
        else if (id == R.id.nav_webmail) {
            // Set the user-agent to the default android user-agent to prevent to desktop display
            // from making some actions un-zoomable
            webSettings.setUserAgentString(defaultUA);

            // Load the schedule webview
            webView.loadUrl(getString(R.string.url_webmail));
        }
        else if (id == R.id.nav_benefits) {
            // Set the user-agent to the default android user-agent to prevent to desktop display
            // from making some actions un-zoomable
            webSettings.setUserAgentString(defaultUA);

            // Load the benefits webview
            webView.loadUrl(getString(R.string.url_benefits));
        }
        else if (id == R.id.nav_taxes) {
            // Set the user-agent to the default android user-agent to prevent to desktop display
            // from making some actions un-zoomable
            webSettings.setUserAgentString(defaultUA);

            // Load the Paperless Employee webview
            webView.loadUrl(getString(R.string.url_taxes));
        }
        else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this,SettingsActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //this.moveTaskToBack(true);
            //this.finish(); // Unnecessary because we're just sticking this in the background?
        }
        else if (id == R.id.nav_about) {
            // Set the user-agent to the default android user-agent
            webSettings.setUserAgentString(defaultUA);

            // Load the about page webview
            webView.loadUrl(getString(R.string.url_about));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // Choose an action type.
                "Main Page", // Define a title for the content shown.
                // If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),

                // Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.nightsstarflake.bullseyeedge/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // Choose an action type
                "Main Page", // Define a title for the content shown
                // If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),

                // Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.nightsstarflake.bullseyeedge/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        // If the user indicated that we can clear the cache, clear it
        //boolean deleteCache = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("PREFS_clearCacheOnExit", true);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean clearCache = sharedPreferences.getBoolean("pref_clearcache", true);

        if (clearCache) {
            // Clear the webview cache and history
            // This is a good thing because it will force a user's schedule to refresh, in case
            // it has been updated on myTime (which the site should theoretically do anyway, but
            // why take chances?)
            webView.clearCache(true);
            webView.clearHistory();

            // Clear all cookies - this will log the user out of everything
            android.webkit.CookieManager cookieManager = CookieManager.getInstance();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.removeAllCookies(new ValueCallback<Boolean>() {
                    // a callback which is executed when the cookies have been removed
                    @Override
                    public void onReceiveValue(Boolean aBoolean) {
                        // Debug log, show removed cookie
                        //Log.d(TAG, "Cookie removed: " + aBoolean);
                    }
                });
            }
            else {
                cookieManager.removeAllCookie();
            }

            // Finally, trim any residual parts of the app cache
            try {
                trimCache();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void trimCache() {
        try {
            File dir = getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // "Handle" exception by doing nothing
        }
    }

    public static boolean deleteDir(File dir)
    {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // As long as the directory exists, delete it
        return dir.delete();
    }
}
