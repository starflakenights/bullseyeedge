package com.nightsstarflake.bullseyeedge;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Starflake Nights on 3/26/2016.
 */
public class WebViewExtension extends WebViewClient {

    private Activity activity = null;
    private ProgressDialog progressDialog = null;
    private WebView webView = null;

    public WebViewExtension(Activity activity, WebView wv) {
        this.activity = activity;
        webView = wv;
        progressDialog = new ProgressDialog(wv.getContext());
        progressDialog.setCancelable(true);
        progressDialog.setTitle("Now Loading");
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
        if(     url.contains("target.com") ||
                url.contains("paperlessemployee.com") ||
                url.contains("targetpayandbenefits.com") ||
                url.contains("hewitt.com") ||
                url.contains("benefithub.com")
          ) return false;

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(intent);
        return true;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (    url.contains("target.com") ||
                url.contains("paperlessemployee.com") ||
                url.contains("targetpayandbenefits.com") ||
                url.contains("hewitt.com") ||
                url.contains("benefithub.com")
            )
        {
            progressDialog.setMessage(url);
            progressDialog.show();
        }
    }

    @Override public void onLoadResource(WebView view, String url) {
        super.onLoadResource(view, url);
    }

    @Override public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if(     url.contains("target.com") ||
                url.contains("paperlessemployee.com") ||
                url.contains("targetpayandbenefits.com") ||
                url.contains("hewitt.com") ||
                url.contains("benefithub.com")
           )
        {
            progressDialog.dismiss();
        }
    }
}
