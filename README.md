# Bullseye Edge
Author: Starflake Nights
- - -
## Description

Bullseye Edge strives to provide Target team members with a "one stop" solution to managing 
their schedule, HR-related information, and webmail. It is an _unofficial_ solution, however,
and will likely never be endorsed by Target Corporation.

No personal information is collected by the app, though you can choose to store your team
member number (and password on older Android platforms) locally. Saving sensitive data locally
presents a large security risk, but it is up to the user to determine if the increased 
ease-of-use is worth that risk.

## Capabilities and Permissions

Currently, this project allows users to access their Target schedule, eHR, and their Outlook
webmail (as well as any links contained therein). It uses Android's built-in Chromium
support, which varies in capability and function depending on Android's version.

Required Permissions: Internet

- - -

### Downloading Source

In order to download the source, which is maintained with the aid of the version-control 
software Git, it is necessary to download a Git client. (It's actually not 
_entirely_ necessary, but highly suggested for retrieving updates. If you're put off by 
installing one more piece of software, you can download a tarball of the most recent release.)

When you have your desired client, follow the general procedure for Git cloning:

    Repository URL: 	https://gitlab.com/starflakenights/bullseyeedge.git
    Git: 				git clone https://gitlab.com/starflakenights/bullseyeedge.git
- - -

### Building the App

For ease of building, the app was created in Android Studio. After the source is downloaded, 
one can simply use Android Studio's "Open" feature to perform the import operation.

If the project is not building, it may be necessary to manually open the SDK manager and 
download the AppCompatv7 and Preferencev7 support libraries.
- - -

### Building Documentation

Documentation will be added in a future release, but it will eventually be buildable via 
Doxygen with the command

	make doc

